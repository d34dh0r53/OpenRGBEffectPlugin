#ifndef EFFECTS_H
#define EFFECTS_H

#include "SpectrumCycling.h"
#include "RainbowWave.h"
#include "StarryNight.h"
#include "GradientWave.h"
#include "Breathing.h"
#include "Rain.h"
#include "Ambient.h"
#include "Visor.h"
#include "AudioVisualizer.h"
#include "AudioSync.h"
#include "Wavy.h"
#include "Lightning.h"

#endif // EFFECTS_H
